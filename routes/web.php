<?php

use Illuminate\Support\Facades\Route;
use \App\Portable;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(request('user')){
	    return view('welcome',[
	    	'portables'=>Portable::where('etat',1)->where('userId',request('user'))->orderBy('nom')->paginate(10)
	    ]);
	}
    return view('welcome',[
    	'portables'=>Portable::where('etat',1)->orderBy('nom')->paginate(10)
    ]);
})->name('Accueil');


Route::get('/register','CompteController@index_inscription')->name('Compte.Inscription');
Route::post('/register','CompteController@inscription')->name('Compte.Inscription');

Route::get('/login','CompteController@index_connexion')->name('Compte.Connexion');
Route::post('/login','CompteController@connexion')->name('Compte.Connexion');


Route::get('/home','PortableController@index')->name('Compte.Home');

Route::get('/deconnexion','CompteController@deconnexion')->name('Compte.Deconnexion');


Route::get('/nouveau-contact','PortableController@create')->name('Contact.Nouveau')->middleware('Connexion');
Route::post('/nouveau-contact','PortableController@store')->name('Contact.Nouveau')->middleware('Connexion');

Route::get('/modifier-contact-{id}','PortableController@edit')->name('Contact.Modifier')->middleware('Connexion');
Route::post('/modifier-contact-{id}','PortableController@update')->name('Contact.Modifier')->middleware('Connexion');

Route::get('/supprimer-contact-{id}','PortableController@delete')->name('Contact.Supprimer')->middleware('Connexion');