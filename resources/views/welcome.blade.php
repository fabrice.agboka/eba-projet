@extends('Layout.Default')
@section('Admin')
<div class="card">
    <div class="card-header bg-transparent">
        <h3 class="mb-0">Liste des contacts</h3>
    </div>
    <div class="card-body">
    	<div class="table-responsive">
	        @include('Layout.Portable')
	    </div>
	</div>
</div>
@endsection