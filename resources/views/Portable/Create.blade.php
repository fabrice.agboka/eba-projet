@extends('Layout.Default')
@section('Admin')
<div class="card">
    <div class="card-header bg-transparent">
        <h3 class="mb-0">Ajouter un nouveau contact</h3>
    </div>
    <div class="card-body">
        <form method="post" action="">
            {{ csrf_field() }}
            <div class="pl-lg-4">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-ligne">Choisir un catégorie</label>
                            <select name="categorie" class="form-control col-md-12">
                                <option value="Famille">Famille</option>
                                <option value="Amis">Amis</option>
                                <option value="Collegues">Collegue</option>
                                <option value="Proches">Proches</option>
                                <option value="Autres">Autres</option>
                            </select>
                        </div>
                        @error('categorie')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-libelle-produit">Nom</label>
                            <input type="text" id="input-libelle-produit" class="form-control" placeholder="Nom" value="{{ old('nom') }}" name="nom">
                        </div>
                        @error('nom')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-numero">Numero</label>
                            <input type="text" id="input-numero" class="form-control" placeholder="numero" value="{{ old('numero') }}" name="numero">
                        </div>
                        @error('numero')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-email">Email</label>
                            <input type="email" id="input-email" class="form-control" placeholder="Email" value="{{ old('email') }}" name="email">
                        </div>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-lg-12" >
                    <div class="text-center">
                      <button  class="btn btn-primary" >Ajouter le contact</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop