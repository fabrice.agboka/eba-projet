@php
    $i=1;
@endphp
<table class="table align-items-center table-flush">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Email</th>
            <th scope="col">Portable</th>
            <th scope="col">Propriétaire</th>
            <th scope="col">Opération</th>
        </tr>
    </thead>
    <tbody class="list">
        @foreach($portables as $value)
            <tr>
                <td class="budget">{{$i++}} </td>
                <td class="budget">{{$value->nom}} </td>
                <td class="budget">{{$value->email}} </td>
                <td class="budget">{{$value->numero}} </td>
                <td class="budget">{{$value->getUser()}} </td>
                @if($value->estPropriétaire())
                <td>
                    <a href="{{ route('Contact.Modifier',['id'=>$value->id]) }}" class="btn btn-success">
                        Modifier
                    </a>
                    <a class="btn btn-danger" href="{{ route('Contact.Supprimer',['id'=>$value->id]) }}">
                        Supprimer
                    </a>
                </td>
                @else
                <td>
                    -
                </td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>
{{$portables->links()}}