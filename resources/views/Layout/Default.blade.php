<!DOCTYPE html>
<html>
    <head>
        {{-- Rafraichir la page chaque 2seconde --}}
        {{-- <meta http-equiv="refresh" content="2"/> --}}
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>{{env('APP_NAME')}} | Test pratique</title>
      

        <link rel="icon" href="Admin/img/brand/favicon.png" type="image/png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
        <link rel="stylesheet" href="Admin/vendors/nucleo/css/nucleo.css" type="text/css">
        <link rel="stylesheet" href="Admin/vendors/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
        <link rel="stylesheet" href="Admin/css/argon.css?v=1.2.0" type="text/css">
    </head>

    <body>
        @include('Layout.Aside')
        <div class="main-content" id="panel">
          @include('Layout.Nav')
          @include('Layout.Banner')
            <div class="container-fluid mt--6">
                    @include('Layout.Message')
                <div class="row justify-content-center">
                    @yield('Admin')
                </div>
                @include('Layout.Footer')
            </div>
        </div>
        <script src="/Admin/vendors/jquery/dist/jquery.min.js"></script>
        <script src="/Admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="/Admin/vendors/js-cookie/js.cookie.js"></script>
        <script src="/Admin/vendors/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="/Admin/vendors/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
        <script src="/Admin/vendors/clipboard/dist/clipboard.min.js"></script>
        <script src="/Admin/js/argon.js?v=1.2.0"></script>
    </body>
</html>