@if(auth()->user())
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="{{ route('Accueil') }}">
                <img src="{{asset('Admin/img/brand/blue.png')}}" class="navbar-brand-img" alt="...">
            </a>
        </div>
        <div class="navbar-inner">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('Compte.Home') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Mes contacts</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('Contact.Nouveau') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Nouveau(s)</span>
                        </a>
                    </li>
                </ul>
                <hr class="my-3">
            </div>
        </div>
    </div>
</nav>
@endif