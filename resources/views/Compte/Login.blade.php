<!doctype html>
<html lang="en">
    <head>
        <title>{{env('APP_NAME')}} {{env('APP_DEV')}} - interface de connexion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Gestock">
        <meta name="description" content="Logiciel de gestion de stock">
        <meta name="url" content="{{ env('APP_URL') }}">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        
        <link href="{{asset('User/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{asset('User/css/material-kit.css?v=1.3.0')}}" rel="stylesheet"/>
        <link href="{{asset('User/Client-for-demo/vertical-nav.css')}}" rel="stylesheet" />
    </head>

    <body class="section-white">
        <div class="page-header header-filter" style="background-image: url('{{asset('../User/img/bg7.jpg')}}'); background-size: cover; background-position: top center;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                        <div class="card card-signup">
                            <form class="form" method="post" action="">
                                {{ csrf_field() }}
                                <div class="header header-primary text-center">
                                    <h4 class="card-title">{{env('APP_NAME')}} {{env('APP_DEV')}}</h4>
                                    <div class="social-line">
                                        <p>Interface de connexion</p>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                                        @error('email')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        <input type="password" name="password" placeholder="Mots de passe..." class="form-control"/>
                                        @error('password')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button class="btn btn-primary">Inscription</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <script src="{{asset('User/js/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('User/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('User/js/material.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('User/js/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('User/js/nouislider.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('User/js/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('User/js/bootstrap-selectpicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('User/js/bootstrap-tagsinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('User/js/jasny-bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('User/js/material-kit.js?v=1.3.0')}}" type="text/javascript"></script>
</html>
