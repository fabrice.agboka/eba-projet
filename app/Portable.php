<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use  \App\User;
use DB;

class Portable extends Model
{
    protected $fillable = [
        'nom', 'email', 'numero','categorie','userId','etat'
    ];

    public function getUser(){
    	$user = User::find($this->userId);
    	return $user->nom;
    }

    public function estPropriétaire(){
    	if(auth()->user() and auth()->user()->id == $this->userId){
    		return true;
    	}

    	return false;
    }

    public function update_all($nom, $email, $numero, $categorie){
        DB::update("update portables set nom=?, numero=?, email=?, categorie=? where id=?",[$nom, $numero, $email, $categorie,$this->id]);
    }

    public function updateCategorie($new){
        if($this->estPropriétaire()){
        	$categorie = $this->categorie.', '.$new;

        	DB::update("update portables set categorie=? where id=?",[$categorie, $this->id]);
        }
    }

    public function delete(){
        if($this->estPropriétaire()){
        	DB::update("update portables set etat=0 where id=?",[$this->id]);
        }
    }
}
