<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \App\User;

class CompteController extends Controller
{
    public function index_inscription(){
    	return view('Compte.Inscription');
    }

    public function inscription(Request $request){
    	$request->validate([
    		'nom' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:4', 'confirmed']
    	]);

    	$user=User::create([
            'nom' => request('nom'),
            'email' => request('email'),
            'password' => Hash::make(request('password'))
        ]);
    	$val=auth()->attempt([
            'email' => request('email'),
            'password' => request('password'),
        ]);

        if($val){
        	return redirect(route('Compte.Home'))->with('success','Bienvenu'.request('nom').' !');
        }
    }

   	public function index_connexion(){
        return view('Compte.Login');
    }

    public function connexion(Request $request){
        $request->validate([
            'email' => ['required', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:4'],
        ]);

        $val=auth()->attempt([
            'email' => request('email'),
            'password' => request('password'),
        ]);
        if($val){
            return redirect(route('Compte.Home'));
        }else{
            return back()->withInput()->withErrors([
                'email'=>'Email ou mot de passe incorrect.'
            ]); 
        }
    }

    function deconnexion(){
        auth()->logout();
        return redirect(route('Accueil'));
    }
}
