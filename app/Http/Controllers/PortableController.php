<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\Portable;

class PortableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Portable.Index',[
            'portables'=>Portable::where('etat',1)->where('userId',auth()->user()->id)->orderBy('nom')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Portable.Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
    		'categorie' => ['required', 'string', 'max:255'],
    		'numero' => ['required', 'string', 'max:255'],
    		'nom' => ['required', 'string', 'max:255'],
    		'email' => ['required', 'email'],
    	]);

    	Portable::create([
    		'categorie'=>request('categorie'),
    		'numero'=>request('numero'),
    		'nom'=>request('nom'),
    		'email'=>request('email'),
    		'userId'=>auth()->user()->id
    	]);


        return back()->with('success','Portable ajouter avec succès!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('Portable.Edit',[
            'portable'=>Portable::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'categorie' => ['required', 'string', 'max:255'],
            'numero' => ['required', 'string', 'max:255'],
            'nom' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
        ]);

        $portable = Portable::find(request('id'));
        $portable->update_all(request('nom'),request('email'),request('numero'),request('categorie'));
        return redirect(route('Compte.Home'))->with('success','Portable mise à jours avec succès!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $portable = Portable::find(request('id'));
        $portable->delete();

        return back()->with('success','Portable supprimer avec succès!');
    }
}
